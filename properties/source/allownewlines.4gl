##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "allownewlines" ATTRIBUTE(BORDER)

 DISPLAY "With DISPLAY, allowNewLines specifies whether the text will be displayed in a single or multiple lines." TO f1
 DISPLAY "With DISPLAY, allowNewLines specifies whether the text will be displayed in a single or multiple lines." TO f2

CALL fgl_getkey()

END MAIN

{MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "allownewlines" ATTRIBUTE(BORDER)

MENU
 COMMAND "input"
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN}

{MAIN
DEFINE f1, f2 STRING,
       ta_1, ta_2 ui.TextArea

OPEN WINDOW w WITH FORM "allownewlines" ATTRIBUTE(BORDER)
LET ta_1 = ui.TextArea.ForName("f1")
LET ta_2 = ui.TextArea.ForName("f2")

MENU
 COMMAND "input"
  CALL apply_theme("anl_input")
  CALL ta_1.setAllowNewLines(0)
  CALL ta_2.setAllowNewLines(1)
  DISPLAY "With f1, allowNewLines=", ta_1.getAllowNewLines()
  DISPLAY "With f2, allowNewLines=", ta_2.getAllowNewLines()
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "display"
  CALL ta_1.setAllowNewLines(0)
  CALL apply_theme("anl_display")
  CALL ta_2.setAllowNewLines(1)
  DISPLAY "With f1, allowNewLines=", ta_1.getAllowNewLines()
  DISPLAY "With f2, allowNewLines=", ta_2.getAllowNewLines()
  DISPLAY "With DISPLAY, allowNewLines specifies whether the text will be displayed in a single or multiple lines." TO f1
  DISPLAY "With DISPLAY, allowNewLines specifies whether the text will be displayed in a single or multiple lines." TO f2
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN}