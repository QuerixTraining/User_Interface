##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

  DEFINE f_combobox ui.ComboBox

  OPEN WINDOW w WITH FORM "text_combobox" ATTRIBUTE(BORDER)
  LET f_combobox = ui.ComboBox.ForName("f1")
  CALL f_combobox.SetEnable(TRUE)
  
CALL fgl_getkey()

END MAIN

{MAIN
	DEFINE cb ui.Combobox
	DEFINE f1 CHAR(20)
	 LET f1 = "value1"
	OPEN WINDOW w WITH FORM "text_combobox" ATTRIBUTE(BORDER)
	LET cb = ui.Combobox.ForName("f1")
    
    input by name f1
	  ON ACTION "SHOW ITEM"
	  	DISPLAY f1
	  ON ACTION "UI.TEXT"
	  	CALL cb.setText("TEXT BY UI")
	  ON ACTION "DISPL value1"
	    DISPLAY "value1" TO f1
	  ON ACTION "DISPL value2"
	    DISPLAY "value2" TO f1
	  ON ACTION "DISPL TEXT"
	    DISPLAY "DISPL TEXT" TO f1
    END INPUT

	MENU
	  COMMAND "SHOW ITEM"
	  	DISPLAY f1

	  COMMAND "UI.TEXT"
	  	CALL cb.setText("TEXT BY UI")

	  COMMAND "DISPL value1"
	    DISPLAY "value1"     TO f1

	  COMMAND "DISPL value2"
	    DISPLAY "value2"     TO f1

	  COMMAND "DISPL TEXT"
	    DISPLAY "DISPL TEXT" TO f1

	  COMMAND "EXIT"
	    EXIT MENU
	END MENU

END MAIN}