##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE bt_ui     ui.Button,
         image_ui   ui.Image,
         bt1 STRING

  OPEN WINDOW n WITH FORM "imagescaling_ui" ATTRIBUTE(BORDER) 

  LET bt_ui = ui.Button.forName("bt1")
  CALL image_ui.setImageUrl("qx://application/middle_lycia.png")
  CALL image_ui.setImageScaling("Both")
  CALL bt_ui.SetImage(image_ui)

  INPUT BY NAME bt1
  
  CALL fgl_getkey()
END MAIN