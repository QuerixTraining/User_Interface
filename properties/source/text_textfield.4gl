##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

  DEFINE tf ui.TextField

  OPEN WINDOW w WITH FORM "text_textfield" ATTRIBUTE(BORDER)
  LET tf = ui.TextField.ForName("tf1")
  CALL tf.SetEnable(TRUE)
  
  MENU
    COMMAND "ui.SetText"
      CALL tf.SetText("ui.SetText")
    COMMAND "DISPLAY...TO"
      DISPLAY "DISPLAY...TO" to tf1
    COMMAND "exit"
      EXIT MENU
  END MENU

CALL fgl_getkey()

END MAIN