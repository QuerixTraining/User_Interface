##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "enablemultiselection" ATTRIBUTE(BORDER)

INPUT BY NAME f1
DISPLAY f1 TO f2

CALL fgl_getkey()
END MAIN

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "enablemultiselection_ui" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "show"
    LET dspl = "enableMultiSelection=", lbx.getEnableMultiSelection()
    DISPLAY dspl TO f2
  COMMAND "change"
    CALL lbx.setEnableMultiSelection(1)
    LET dspl = "enableMultiSelection=", lbx.getEnableMultiSelection()
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}