##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

--Programs were created by Eugenia Pyzina, 07.04.2016

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "tooltip" ATTRIBUTE(BORDER)

MENU
  ON ACTION "enter"
    INPUT BY NAME f1
      END INPUT
    INPUT BY NAME f2
      END INPUT
  ON ACTION "exit"
    EXIT MENU
END MENU

END MAIN

{MAIN
DEFINE bt1, f2, dspl STRING,
       bt ui.Button

OPEN WINDOW w WITH FORM "tooltip_ui" ATTRIBUTE(BORDER)
LET bt = ui.Button.ForName("bt1")

MENU
  COMMAND "get"
    LET dspl = "Tooltip text - ", bt.GetTooltip()
    DISPLAY dspl TO f2
  COMMAND "set"
    CALL bt.SetTooltip("My new tooltip")
    LET dspl = "Tooltip text - ", bt.GetTooltip()
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}