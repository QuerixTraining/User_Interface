##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

--Programs were created by Eugenia Pyzina, 19-21.04.2016

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "listbox" ATTRIBUTE(BORDER)

INPUT BY NAME f1
DISPLAY f1 TO f2

CALL fgl_getkey()
END MAIN

--SetListBoxValues and GetListBoxValues

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_03" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "get"
    LET dspl = lbx.getListBoxValues()
    DISPLAY dspl TO f2
  COMMAND "set"
    CALL lbx.setListBoxValues(["LyciaDesktop", "LyciaWeb", "LyciaTouch"])
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

--SetSelectedItems and GetSelectedItems

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_03" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "get"
    LET dspl = lbx.getSelectedItems()
    DISPLAY dspl TO f2
  COMMAND "set"
    CALL lbx.setSelectedItems("new value")
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

-- AddItem and RemoveItem

{MAIN
DEFINE f1 STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_01" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "add"
    CALL lbx.AddItem("4", "Four")
    CALL lbx.AddItem("5", "Five")
  COMMAND "remove"
    CALL lbx.RemoveItem("4", "Four")
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

-- Clear

{MAIN
DEFINE f1 STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_01" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "clear"
    CALL lbx.Clear()
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

-- GetItemCount

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_02" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "count"
    LET dspl = lbx.GetItemCount()
    DISPLAY dspl TO f2
  COMMAND "add"
    CALL lbx.AddItem("Four", "")
    CALL lbx.AddItem("Five", "")
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

-- GetItemName

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_02" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "get name"
    LET dspl = lbx.GetItemName(2)
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

-- GetItemText

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_02" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "get text"
    LET dspl = lbx.GetItemText(2)
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

-- GetTag

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "get tag"
    LET dspl = lbx.GetTag()
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

-- GetTextOf

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_02" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "get text"
    LET dspl = lbx.GetTextOf("3", "Three")
    DISPLAY dspl TO f2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}

{MAIN
DEFINE f1, f2, dspl STRING,
       lbx ui.ListBox

OPEN WINDOW w WITH FORM "listbox_ui_02" ATTRIBUTE(BORDER)

LET lbx = ui.ListBox.Forname("f1")

MENU
  COMMAND "add"
    CALL lbx.AddItem("Four", "")
    CALL lbx.AddItem("Five", "")
  COMMAND "remove"
    CALL lbx.RemoveItem("Five", "")
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN}