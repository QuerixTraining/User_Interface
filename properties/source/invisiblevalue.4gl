##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "invisibleValue" ATTRIBUTE(BORDER)

MENU
 COMMAND "input"
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "exit"
  EXIT MENU
END MENU

END MAIN}

MAIN
DEFINE f1, f2 STRING,
       tf_1, tf_2 ui.TextField

OPEN WINDOW w WITH FORM "invisibleValue" ATTRIBUTE(BORDER)

LET tf_1 = ui.TextField.ForName("f1")
LET tf_2 = ui.TextField.ForName("f2")

MENU
 COMMAND "input"
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "change & input"
  CALL tf_1.setInvisibleValue(1)
  CALL tf_2.setInvisibleValue(0)
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN