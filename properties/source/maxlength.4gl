##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "maxLength" ATTRIBUTE(BORDER)

MENU
 COMMAND "input"
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "exit"
  EXIT MENU
END MENU

END MAIN}

{MAIN
DEFINE f1, f2 STRING,
       ta_1, ta_2 ui.TextArea

OPEN WINDOW w WITH FORM "maxLength" ATTRIBUTE(BORDER)

LET ta_1 = ui.TextArea.ForName("f1")
CALL ta_1.setMaxLength(10)
LET ta_2 = ui.TextArea.ForName("f2")
CALL ta_2.setMaxLength(5)

MENU
 COMMAND "input"
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN}

MAIN

DEFINE f1, f2 STRING,
       ta_1, ta_2 ui.TextArea

OPEN WINDOW w WITH FORM "maxLength" ATTRIBUTE(BORDER)

LET ta_1 = ui.TextArea.ForName("f1")
LET ta_2 = ui.TextArea.ForName("f2")

MENU
 COMMAND "display"
  DISPLAY "With f1, maxLength=", ta_1.getMaxLength(), "."
  DISPLAY "With f2, maxLength=", ta_2.getMaxLength(), "."
 COMMAND "change"
  CALL ta_1.setMaxLength(10)
  CALL ta_2.setMaxLength(5)
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN