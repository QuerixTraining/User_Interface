##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
 DEFINE lb1, lb2 STRING
 
 OPEN WINDOW w WITH FORM "isdynamic" ATTRIBUTE(BORDER)
 
 MENU
 COMMAND "1st display"
   DISPLAY "1st display" TO lb1 
   --DISPLAY "qx://application/01.png" TO lb1 
   DISPLAY "1st display" TO lb2 
   --DISPLAY "qx://application/01.png" TO lb1 
 COMMAND "2nd display"
   DISPLAY "2nd display" TO lb1
   DISPLAY "qx://application/2nd_display.png" TO lb1 
   DISPLAY "2nd display" TO lb2
   DISPLAY "qx://application/2nd_display.png" TO lb2 
 COMMAND "EXIT"
   EXIT MENU
 END MENU
END MAIN

