##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "isPasswordMask" ATTRIBUTE(BORDER)

MENU
 COMMAND "input"
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "display"
  DISPLAY "isPasswordMask = false" TO f1
  DISPLAY "isPasswordMask = true" TO f2
 COMMAND "exit"
  EXIT MENU
END MENU

END MAIN

{MAIN
DEFINE f1, f2 STRING,
       tf_1, tf_2 ui.TextField

OPEN WINDOW w WITH FORM "isPasswordMask" ATTRIBUTE(BORDER)

LET tf_1 = ui.TextField.ForName("f1")
LET tf_2 = ui.TextField.ForName("f2")

CALL tf_1.setIsPasswordMask(1)
CALL tf_2.setIsPasswordMask(0)

MENU
 COMMAND "change & input"
  CALL tf_1.setIsPasswordMask(0)
  CALL tf_2.setIsPasswordMask(1)
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "change & display"
  CALL tf_1.setIsPasswordMask(1)
  CALL tf_2.setIsPasswordMask(0)
  DISPLAY "displayed text" TO f1
  DISPLAY "displayed text" TO f2
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN}