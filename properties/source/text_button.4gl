##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{MAIN

DEFINE bt1 CHAR

OPEN WINDOW w WITH FORM "text_button" ATTRIBUTE(BORDER)

DISPLAY "!" TO bt1

CALL fgl_getkey()

END MAIN}

MAIN

  DEFINE bt ui.Button

  OPEN WINDOW w WITH FORM "text_button" ATTRIBUTE(BORDER)
  LET bt = ui.Button.ForName("bt1")
  CALL bt.SetEnable(TRUE)
  
  MENU
    COMMAND "ui.SetText"
      CALL bt.SetText("ui.SetText")
    COMMAND "DISPLAY...TO"
      DISPLAY "DISPLAY...TO" to bt1
    COMMAND "exit"
      EXIT MENU
  END MENU

CALL fgl_getkey()

END MAIN