##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{MAIN

OPEN WINDOW w WITH FORM "text_label" ATTRIBUTE(BORDER)

CALL fgl_getkey()

END MAIN}

MAIN

  DEFINE lb ui.Label

  OPEN WINDOW w WITH FORM "text_label" ATTRIBUTE(BORDER)
  LET lb = ui.Label.ForName("lb1")
  
  MENU
    COMMAND "ui.SetText"
      CALL lb.SetText("ui.SetText")
    COMMAND "DISPLAY...TO"
      DISPLAY "DISPLAY...TO" to lb1
    COMMAND "exit"
      EXIT MENU
  END MENU

END MAIN