##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

--Programs were created by Eugenia Pyzina, 06.04.2016

{MAIN
DEFINE f1 STRING

OPEN WINDOW w WITH FORM "hidelabels" ATTRIBUTE(BORDER)

MENU
  ON ACTION "lycia"
    DISPLAY "Lycia" TO f1
  ON ACTION "birt"
    DISPLAY "BIRT" TO f1
END MENU

CALL fgl_getkey()

END MAIN}

MAIN
DEFINE f1, dspl STRING,
       tb ui.Toolbar

OPEN WINDOW w WITH FORM "hidelabels" ATTRIBUTE(BORDER)
LET tb = ui.Toolbar.ForName("toolbarMain1")

MENU
 COMMAND "hide"
  CALL tb.setHideLabels(1)
  LET dspl = "hideLabels=", tb.getHideLabels()
  DISPLAY dspl TO f1
 COMMAND "show"
  CALL tb.setHideLabels(0)
  LET dspl = "hideLabels=", tb.getHideLabels()
  DISPLAY dspl TO f1
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN