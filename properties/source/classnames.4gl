##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{MAIN
DEFINE lb1, lb2 STRING

OPEN WINDOW w WITH FORM "classnames" ATTRIBUTE(BORDER)

DISPLAY "classnames=first_class" TO lb1
DISPLAY "classnames=second_class" TO lb2

CALL fgl_getkey()
END MAIN}


MAIN
DEFINE lb1, lb2 STRING,
       lb_1, lb_2 ui.Label

OPEN WINDOW w WITH FORM "classnames_ui" ATTRIBUTE(BORDER)
LET lb_1 = ui.Label.ForName("lb1")
LET lb_2 = ui.Label.ForName("lb2")

MENU
 COMMAND "get"
  DISPLAY "With lb1, classNames=", lb_1.getClassNames()
  DISPLAY "With lb2, classNames=", lb_2.getClassNames()
  DISPLAY "lb1" TO lb1
  DISPLAY "lb2" TO lb2
 COMMAND "set"
  CALL lb_1.setClassNames("first_class")
  CALL lb_2.setClassNames("second_class")
  DISPLAY "With lb1, classNames=", lb_1.getClassNames()
  DISPLAY "With lb2, classNames=", lb_2.getClassNames()
  DISPLAY "lb1" TO lb1
  DISPLAY "lb2" TO lb2
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN