MAIN
 DEFINE f1 STRING
 DEFINE tf ui.Textfield
 DEFINE loc ui.Locale
 DEFINE txtAlign ui.TextAlignment
	OPEN WINDOW w WITH FORM "locale_ui" ATTRIBUTE(BORDER)

	#initialize Locale
 	CALL loc.SetCountry("US")
	CALL loc.SetDirection("RTL")
	CALL loc.SetLanguage( "EN")
	CALL loc.SetVariant("cp1251")
	#align text by right side for RTL
	CALL txtAlign.SetHorizontalTextAlignment("Right")
	CALL txtAlign.SetVerticalTextAlignment("Default")
	#apply all changes to TextField
	LET tf = ui.Textfield.Forname("f1")
	CALL tf.SetTextAlignment(txtAlign)
    CALL tf.SetLocale(loc) 

	INPUT BY NAME f1
END MAIN