##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE f1, f2 STRING

OPEN WINDOW w WITH FORM "autonext" ATTRIBUTE(BORDER)

MENU
 COMMAND "input"
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN


{MAIN
DEFINE f1, f2 STRING,
       ta ui.TextArea

OPEN WINDOW w WITH FORM "autonext" ATTRIBUTE(BORDER)

LET ta = ui.TextArea.ForName("f1")

MENU
 COMMAND "input"
  CALL ta.setAutonext(1)
  INPUT BY NAME f1 WITHOUT DEFAULTS
  INPUT BY NAME f2 WITHOUT DEFAULTS
 COMMAND "display"
  DISPLAY "autonext is set to ", ta.getAutonext(), "."
 COMMAND "exit"
  EXIT MENU
END MENU 

END MAIN}