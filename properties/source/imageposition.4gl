##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{MAIN
  DEFINE lb STRING

  OPEN WINDOW n WITH FORM "imageposition" ATTRIBUTE(BORDER) 
  DISPLAY "Text" TO lb
  CALL fgl_getkey()
END MAIN}

MAIN
  DEFINE bt_ui     ui.Button,
         image_ui   ui.Image,
         bt1 STRING,
         bt2 CHAR(16)

  OPEN WINDOW n WITH FORM "imageposition_ui" ATTRIBUTE(BORDER) 
  LET bt2 = "Top"  
  LET bt_ui = ui.Button.forName("bt1")
  CALL image_ui.setImageUrl("qx://application/large_lycia.png")
--  CALL image_ui.setImageScaling("Both")
  CALL image_ui.setImagePosition("Top")
  CALL bt_ui.SetImage(image_ui)
  CALL bt_ui.SetText("Text")
  
  MENU
    ON ACTION "act_bt"
      CASE bt2
        WHEN "Top"
          CALL image_ui.setImagePosition("Bottom")
          CALL bt_ui.SetImage(image_ui)
          CALL bt_ui.SetText("Text")
          LET bt2 = "Bottom"

        WHEN "Bottom"
          CALL image_ui.setImagePosition("Left")
          CALL bt_ui.SetImage(image_ui)
          CALL bt_ui.SetText("Text")
          LET bt2 = "Left"
  
        WHEN "Left"
          CALL image_ui.setImagePosition("Right")
          CALL bt_ui.SetImage(image_ui)
          CALL bt_ui.SetText("Text")
          LET bt2 = "Right"
  
        WHEN "Right"
          CALL image_ui.setImagePosition("Top")
          CALL bt_ui.SetImage(image_ui)
          CALL bt_ui.SetText("Text")
          LET bt2 = "Top"
      END CASE
  
    ON ACTION "act_display"
      DISPLAY bt2
  
    ON ACTION "act_exit"
      EXIT MENU
  END MENU

 -- CALL fgl_getkey()
END MAIN



{MAIN
  DEFINE f1 STRING

  OPEN WINDOW n WITH FORM "imageposition_rbli" ATTRIBUTE(BORDER) 
  INPUT BY NAME f1
  CALL fgl_getkey()
END MAIN}