##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

DEFINE calendar_var DATE

OPEN WINDOW w WITH FORM "calendar_input_from" ATTRIBUTE(BORDER)

MENU
  COMMAND "input"
    INPUT calendar_var FROM calendar_widget_1
  COMMAND "display"
    DISPLAY calendar_var TO calendar_widget_2
  COMMAND "exit"
    EXIT MENU
END MENU

END MAIN