##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
MAIN
OPEN WINDOW w WITH FORM "SimpleWidgets" ATTRIBUTE(BORDER)
MENU
	COMMAND "CAL_TXT"
		DISPLAY "12/12/2016" TO cal
	COMMAND "BT_TXT"
		DISPLAY "New button text" TO bt
	COMMAND "BT_IMG"
		DISPLAY "qx://application/im1.ico" TO bt
	COMMAND "BT_ACTIVE"
		DISPLAY "!" TO bt
	COMMAND "BT_INACTIVE"
		DISPLAY "*" TO bt
	COMMAND "TF_TXT"
		DISPLAY "New text in TextField" TO tf
	COMMAND "EXIT"
		EXIT MENU
END MENU

END MAIN


















