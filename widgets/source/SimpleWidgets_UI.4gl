##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
MAIN
 DEFINE tcal ui.Calendar
 DEFINE tbt ui.Button
 DEFINE ttf ui.Textfield
 DEFINE img ui.Image
 DEFINE Event ui.BackgroundServerEventHandler

 OPEN WINDOW w WITH FORM "SimpleWidgets" ATTRIBUTE(BORDER)

 LET tcal = ui.Calendar.ForName("cal")
 LET tbt  = ui.Button.ForName("bt")
 LET ttf  = ui.Textfield.ForName("tf")
 MENU
	COMMAND "CAL_TXT"
		CALL tcal.SetText("12/12/2016")
	COMMAND "BT_TXT"
		CALL tbt.SetText("New button text")
	COMMAND "BT_IMG"
		CALL img.SetImageUrl("qx://application/im1.ico")
		CALL tbt.SetImage(img)
	COMMAND "BT_ACTIVE"
		CALL tbt.SetEnable(TRUE)	
	COMMAND "BT_INACTIVE"
		CALL tbt.SetEnable(FALSE)
	COMMAND "BT_SET_ACTION"
		LET Event = ui.BackgroundServerEventHandler.Create()
		CALL Event.SetCallBackAction("buttonAction")
		CALL tbt.SetOnMouseClick(Event)
	COMMAND "TF_TXT"
		CALL ttf.SetText("New text in TextField")
	ON ACTION "buttonAction"
		DISPLAY "Button action is triggered"
	COMMAND "EXIT"
		EXIT MENU
 END MENU
END MAIN