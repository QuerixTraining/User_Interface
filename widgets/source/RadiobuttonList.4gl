##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
MAIN
DEFINE rbl ui.RadioGroup
DEFINE rbl_items DYNAMIC ARRAY OF ui.Radio
OPEN WINDOW w WITH FORM "RadiobuttonList" ATTRIBUTE(BORDER)
LET rbl = ui.RadioGroup.ForName("f1")
MENU
	COMMAND "CB_ADD"
		LET rbl_items[1] = ui.Radio.Create("rbli1")
		CALL rbl_items[1].SetTitle("Item text 1")
		CALL rbl_items[1].SetValue("1")
		LET rbl_items[2] = ui.Radio.Create("rbli2")
		CALL rbl_items[2].SetTitle("Item text 2")
		CALL rbl_items[2].SetValue("2")
		LET rbl_items[3] = ui.Radio.Create("rbli3")
		CALL rbl_items[3].SetTitle("Item text 3")
		CALL rbl_items[3].SetValue("3")
		CALL rbl.SetRadios(rbl_items)   
	COMMAND "CB_DEL_2"
		LET rbl_items = rbl.GetRadios()
		CALL rbl_items.delete(2)
		CALL rbl.SetRadios(rbl_items)
	COMMAND "CB_DEL_ALL"
		LET rbl_items = rbl.GetRadios()
		CALL rbl_items.clear()
		CALL rbl.SetRadios(rbl_items)
	COMMAND "UPDATE_IT"
		LET rbl_items = rbl.GetRadios()
		CALL rbl_items[1].SetTitle("New Title")
		CALL rbl_items[1].SetValue("New Value")
		CALL rbl.SetRadios(rbl_items)
	COMMAND "INSERT_IT"
		LET rbl_items = rbl.GetRadios()
		CALL rbl_items.Insert(2,ui.Radio.Create("rbli4"))
		CALL rbl_items[2].SetTitle("Inserted item")
		CALL rbl_items[2].SetValue("Inserted value")
		CALL rbl.SetRadios(rbl_items)
	COMMAND "APPEND_IT"
		LET rbl_items = rbl.GetRadios()
		CALL rbl_items.append(ui.Radio.Create("rbli20"))
		CALL rbl_items[rbl_items.getLength()].SetTitle("Appended Item")
		CALL rbl_items[rbl_items.getLength()].SetValue("Appended value")
		CALL rbl.SetRadios(rbl_items)
	COMMAND "EXIT"
		EXIT MENU
END MENU
CLOSE WINDOW w
END MAIN