##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
MAIN
DEFINE cb ui.Combobox
DEFINE cb_items DYNAMIC ARRAY OF STRING
OPEN WINDOW w WITH FORM "ComboBox" ATTRIBUTE(BORDER)
LET cb = ui.Combobox.ForName("f1")
MENU
	COMMAND "CB_ADD"
		CALL cb.AddItem("value1")
		CALL cb.AddItem("value2")
	COMMAND "CB_DEL"
		CALL cb.removeItem("value1")
		CALL cb.removeItem("value2")
	COMMAND "ADD_3_ITEMS"
		LET cb_items[1] = "value1"
		CALL cb_items.append("value2")
		CALL cb_items.insert(3,"value3")
		CALL cb.SetComboboxValues(cb_items)
	COMMAND "INSERT_IT"
		CALL cb_items.insert(2,"inserted")
		CALL cb.SetComboboxValues(cb_items)
	COMMAND "APPEND_IT"
		CALL cb_items.append("appended")
		CALL cb.SetComboboxValues(cb_items)
	COMMAND "DELETE_THIRD_ITEM"
		CALL cb_items.delete(3)
		CALL cb.SetComboboxValues(cb_items)
	COMMAND "EXIT"
		EXIT MENU
END MENU
CLOSE WINDOW w

OPEN WINDOW w WITH FORM "ComboBox" ATTRIBUTE(BORDER)
MENU
	COMMAND "INSERT_IT"
		CALL fgl_list_insert("f1",1,"value1")
		CALL fgl_list_insert("f1",2,"value2")
		CALL fgl_list_insert("f1",3,"value3")
		CALL fgl_list_insert("f1",4,"value4")
	COMMAND "UPDATE_1"
		CALL fgl_list_set("f1",1,"new_value")
	COMMAND "DELETE_2_TO_4"
		CALL fgl_list_clear("f1",2,4)
	COMMAND "EXIT"
		EXIT MENU
END MENU

END MAIN