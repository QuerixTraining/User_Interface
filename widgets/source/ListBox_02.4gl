##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
MAIN
DEFINE lb ui.ListBox
DEFINE lb_items DYNAMIC ARRAY OF STRING
OPEN WINDOW w WITH FORM "ListBox_02" ATTRIBUTE(BORDER)
LET lb = ui.Listbox.ForName("f1")
MENU
	COMMAND "CB_ADD"
		CALL lb.AddItem("value1")
		CALL lb.AddItem("value2")
	COMMAND "CB_DEL"
		CALL lb.removeItem("value1")
		CALL lb.removeItem("value2")
	COMMAND "ADD_3_ITEMS"
		LET lb_items[1] = "value1"
		CALL lb_items.append("value2")
		CALL lb_items.insert(3,"value3")
		CALL lb.SetListboxValues(lb_items)
	COMMAND "INSERT_IT"
		CALL lb_items.insert(2,"inserted")
		CALL lb.SetListboxValues(lb_items)
	COMMAND "APPEND_IT"
		CALL lb_items.append("appended")
		CALL lb.SetListboxValues(lb_items)
	COMMAND "DELETE_THIRD_ITEM"
		CALL lb_items.delete(3)
		CALL lb.SetListboxValues(lb_items)
	COMMAND "EXIT"
		EXIT MENU
END MENU
CLOSE WINDOW w
END MAIN