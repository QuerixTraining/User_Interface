##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DATABASE cms
MAIN
DEFINE start_date, end_date DATE
	CALL fgl_putenv("DBDATE=dmy4/")
	CALL populate_data() 
	LET start_date = "01/01/1991"
	LET end_date = TODAY	
	OPEN WINDOW w WITH FORM "calendar_list_cursor" ATTRIBUTE(BORDER)
	DIALOG
		INPUT BY NAME start_date, end_date WITHOUT DEFAULTS
			AFTER FIELD end_date NEXT FIELD start_date
		END INPUT
		ON ACTION "Show"
			CALL fgl_dialog_update_data()
			CALL show_list(start_date, end_date)
		ON ACTION "EXIT"
			EXIT DIALOG
	END DIALOG
END MAIN

##############################
FUNCTION show_list(start_date, end_date)
 DEFINE start_date, end_date DATE
 DEFINE cur CURSOR
 DEFINE tStr STRING
 DEFINE res_rec RECORD 
					field1 DATE,
					field2 CHAR(20)	
				END RECORD
	CALL cur.Declare("SELECT * FROM brthd WHERE b_date BETWEEN ? AND ?",1)
	CALL cur.SetParameters(start_date, end_date)
	CALL cur.SetResults(res_rec.*)
	CALL cur.Open()
	WHILE TRUE
		IF cur.FetchNext() THEN EXIT WHILE END IF --get one by one row while we have returned data by declared query
		LET tStr = tStr,"Name: ",trim(res_rec.field2),"  Birth Date:",trim(res_rec.field1),"\n"
	END WHILE
	DISPLAY tStr TO list
	CALL cur.Close()
	CALL cur.Free()
END FUNCTION
##############################
FUNCTION populate_data()
 DEFINE tdate DATE
 DEFINE tuser CHAR(50)
 DEFINE i INTEGER
	CREATE TEMP TABLE brthd(
		b_date DATE,
		u_name CHAR(50)
	)
LET  tdate = "01.01.1900"
FOR i=1 TO 1000
	LET tdate = tdate+1 UNITS YEAR
	LET tuser = "User",trim(i)
	INSERT INTO brthd VALUES (tdate,tuser)
END FOR
END FUNCTION