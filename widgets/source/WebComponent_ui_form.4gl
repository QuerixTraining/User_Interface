##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#Usage: 
# - Run app
# - Type any place you want
# - Click "Find" button

MAIN
 DEFINE win ui.Window
 DEFINE wc ui.WebComponent
 DEFINE grid ui.GridPanel
 DEFINE gr   ui.GridRowDefinition
 DEFINE stringForWc STRING
 DEFINE tf ui.Textfield
 DEFINE emlarr dynamic array of ui.AbstractUIElement
 DEFINE rLocation ui.GridItemLocation
 #INPUT varialbes
 DEFINE tf1 STRING
 OPEN WINDOW w1 WITH FORM "WebComponent_ui_form" ATTRIBUTE(BORDER)
 
 #Initialize grid container
 LET grid = ui.GridPanel.ForName("rootContainer")
 #set pref size to grid container
 CALL grid.SetPreferredSize(["600px","600px"])
 #set twio gerid rows to existent grid panel
 LET gr.GridLengthValue = "" --means fit to webcomponent size
 CALL grid.SetGridRowDefinitions([gr,gr])

 #Create textfield
 LET tf = ui.Textfield.Create("tf1")
 #define TextField location in grid
 LET rLocation.GridHeight = 1
 LET rLocation.GridWidth = 1
 LET rLocation.GridX = 0
 LET rLocation.GridY = 0
 CALL tf.SetGridItemLocation(rLocation)
 
 #Create webComponent
 LET wc = ui.WebComponent.Create("wc1")
 #set pref size to WebComponent
 CALL wc.SetPreferredSize(["500px","500px"])
 #define WebComponent location in grid
 LET rLocation.GridHeight = 1
 LET rLocation.GridWidth = 1
 LET rLocation.GridX = 0
 LET rLocation.GridY = 1
 CALL wc.SetGridItemLocation(rLocation)
 CALL wc.SetHorizontalAlignment("Stretch")
 CALL wc.SetVerticalAlignment("Stretch")
 CALL wc.SetComponentType("google-map")
 
 CALL emlarr.append(tf)
 CALL emlarr.append(wc)
 
 #Add WebComponent to Grid
 CALL grid.SetItems(emlarr) 

 INPUT BY NAME tf1
	AFTER FIELD tf1 NEXT FIELD tf1
 	ON ACTION "Find"
 		CALL fgl_dialog_update_data()
 		DISPLAY tf1 TO wc1
 END INPUT
 

END MAIN