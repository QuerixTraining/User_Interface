##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

--Programs were created by Eugenia Pyzina, 12.04.2016

MAIN
DEFINE f1 STRING

OPEN WINDOW w WITH FORM "toolbarbutton" ATTRIBUTE(BORDER)

MENU
  ON ACTION "display_text"
    DISPLAY "Hello, world!" TO f1
  ON ACTION "clear_text"
    CLEAR f1
END MENU

CALL fgl_getkey()
END MAIN

{MAIN
DEFINE f1 STRING

OPEN WINDOW w WITH FORM "toolbarbutton_with_class" ATTRIBUTE(BORDER)

MENU
  ON ACTION "display_text"
    DISPLAY "Hello, world!" TO f1
  ON ACTION "clear_text"
    CLEAR f1
END MENU

CALL fgl_getkey()
END MAIN}


{MAIN
DEFINE f1 STRING

OPEN WINDOW w WITH FORM "toolbarbutton_appear_on_focus" ATTRIBUTE(BORDER)

MENU
  COMMAND "input"
    INPUT BY NAME f1
      ON ACTION "show_yes" INFIELD f1
        DISPLAY "yes"
      ON ACTION "show_no" INFIELD f1
        DISPLAY "no"
    END INPUT
  COMMAND "exit"
    EXIT MENU
END MENU
END MAIN}