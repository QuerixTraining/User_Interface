##########################################################################
# User Interface Project			                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

--Programs were created by Eugenia Pyzina, 16-18.03.2016

-- 01 - opens a new window to the place holder by OPEN WINDOW ... INTO "placeholder_id" (one window)

MAIN
DEFINE ph STRING

OPEN WINDOW w1 WITH FORM "ph_01" ATTRIBUTE(BORDER)
OPEN WINDOW w2 INTO "ph"  WITH FORM "ph_01_into"
CALL apply_theme("ph_theme_01")

CALL fgl_getkey()
END MAIN

-- 02 - opens a new window to the place holder by OPEN WINDOW ... INTO "placeholder_id" (three windows)

{MAIN
DEFINE ph, ph1 STRING

OPEN WINDOW w1 WITH FORM "ph_02a" ATTRIBUTE(BORDER)
OPEN WINDOW w4 INTO "ph"  WITH FORM "ph_02a_into"
CALL apply_theme("ph_theme_02")
CALL fgl_getkey()

OPEN WINDOW w2 WITH FORM "ph_02b" ATTRIBUTE(BORDER)
OPEN WINDOW w5 INTO "ph"  WITH FORM "ph_02b_into"
CALL fgl_getkey()

OPEN WINDOW w3 WITH FORM "ph_02c" ATTRIBUTE(BORDER)
OPEN WINDOW w6 INTO "ph1"  WITH FORM "ph_02c_into"
CALL fgl_getkey()

OPEN WINDOW w7 INTO "ph"  WITH FORM "ph_02d_into"
CALL fgl_getkey()

END MAIN}


-- 03 - opens a new window to the place holder by OPEN WINDOW ... AT "window_name.placeholder_id" 

{MAIN
DEFINE ph STRING

OPEN WINDOW w1 WITH FORM "ph_03" ATTRIBUTE(BORDER)
OPEN WINDOW w3 INTO "ph"  WITH FORM "ph_03a_into"
CALL apply_theme("ph_theme_03")
CALL fgl_getkey()

OPEN WINDOW w2 WITH FORM "ph_03" ATTRIBUTE(BORDER)
OPEN WINDOW w4 INTO "ph"  WITH FORM "ph_03b_into"
CALL fgl_getkey()

OPEN WINDOW w5 INTO "w1.ph"  WITH FORM "ph_03c_into"
CALL fgl_getkey()

END MAIN}



-- 04 - opens a new window to the place holder by OPEN WINDOW ... INTO "window_name.placeholder_id" with placeholder's id set in theme

{MAIN
DEFINE ph STRING

OPEN WINDOW w1 WITH FORM "ph_04" ATTRIBUTE(BORDER)
OPEN WINDOW w4 INTO "ph"  WITH FORM "ph_04a_into"
CALL apply_theme("ph_theme_04")
CALL fgl_getkey()

OPEN WINDOW w2 WITH FORM "ph_04" ATTRIBUTE(BORDER)
OPEN WINDOW w5 INTO "ph"  WITH FORM "ph_04b_into"
CALL fgl_getkey()

OPEN WINDOW w3 WITH FORM "ph_04" ATTRIBUTE(BORDER)
OPEN WINDOW w6 INTO "ph"  WITH FORM "ph_04c_into"
CALL fgl_getkey()

OPEN WINDOW w7 INTO "w1.ph"  WITH FORM "ph_04d_into"
CALL fgl_getkey()

END MAIN}



-- 05 - opens a new window to the place holder by OPEN WINDOW ... AT N,N

{MAIN

OPEN WINDOW w1 WITH FORM "ph_05" ATTRIBUTE(BORDER)
OPEN WINDOW w2 AT 2,2 WITH FORM "ph_05_into"
CALL apply_theme("ph_theme_05")

CALL fgl_getkey()
END MAIN}



-- 06 - displays text to the place holder by DISPLAY ... AT N,N

{MAIN

OPEN WINDOW w1 WITH FORM "ph_06" ATTRIBUTE(BORDER)
DISPLAY "DISPLAY to PlaceHolder" AT 2,2
CALL apply_theme("ph_theme_06")

CALL fgl_getkey()
END MAIN}









